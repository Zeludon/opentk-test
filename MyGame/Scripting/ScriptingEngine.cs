﻿using System;
using Jint;
using System.IO;

namespace MyGame.Scripting
{
    class JavascriptEngine
    {
        private Engine engine;

        public JavascriptEngine()
        {
            engine = new Engine((cfg) => cfg.TimeoutInterval(TimeSpan.FromSeconds(5)));
        }

        public object ExecuteFile(string path)
        {
            if (!File.Exists(path)) throw new FileNotFoundException();
            string script = File.ReadAllText(path);
            object result;
            try
            {
                result = engine.Execute(script).GetCompletionValue().ToObject();
                return result;
            }
            catch (Jint.Runtime.JavaScriptException Ex)
            {
                return Ex.Message;
            }
            
        }

        public object Execute(string code)
        {
            object result;
            try
            {
                result = engine.Execute(code).GetCompletionValue().ToObject();
                return result;
            }
            catch (Jint.Runtime.JavaScriptException Ex)
            {
                return Ex.Message;
            }
        }
    }
}
