﻿using Config.Net;
using MyGame.Config;
using Serilog;
using MyGame.Scripting;

namespace MyGame
{
    static class Program
    {
        public static IMySettings settings;
        static void Main()
        {


            Log.Logger = new LoggerConfiguration()
              .WriteTo.Console()
              .WriteTo.File("log.txt", restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Warning)
              .CreateLogger();

            Log.Information("Logger Inititialized.");


            settings = new ConfigurationBuilder<IMySettings>()
                .UseJsonFile("Config/config.json")
                .Build();

            Log.Information("Settings Built.");

            Log.Information("Launching Game.");
            using (var game = new Game(settings.ScreenWidth, settings.ScreenHeight))
            {
                game.Run(60);
            }

            Log.CloseAndFlush();
        } 
    }
}