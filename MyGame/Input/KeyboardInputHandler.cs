﻿using System.Collections.Generic;
using OpenTK;
using OpenTK.Input;
using MyGame.Config;
using Config.Net;

namespace MyGame.Input
{
    class KeyboardInputHandler
    {
        private List<Key> keysDown;
        private List<Key> keysDownLast;
        private List<MouseButton> buttonsDown;
        private List<MouseButton> buttonsDownLast;

        public IKeybindSettings KeyBinds;

        //public KeyboardState KeyboardState { get; private set; }

        public KeyboardInputHandler(GameWindow game)
        {
            KeyBinds = new ConfigurationBuilder<IKeybindSettings>()
                .UseTypeParser(new KeyParser())
                .UseJsonFile("Config/keys.json")
                .Build();

            keysDown = new List<Key>();
            keysDownLast = new List<Key>();
            buttonsDown = new List<MouseButton>();
            buttonsDownLast = new List<MouseButton>();

            game.KeyDown += Game_KeyDown;
            game.KeyUp += Game_KeyUp;
            game.MouseDown += Game_MouseDown;
            game.MouseUp += Game_MouseUp;
            game.UpdateFrame += Update;
        }

        private void Game_MouseUp(object sender, MouseButtonEventArgs e)
        {
            while (buttonsDown.Contains(e.Button))
                buttonsDown.Remove(e.Button);
        }
        private void Game_MouseDown(object sender, MouseButtonEventArgs e) => buttonsDown.Add(e.Button);

        private void Game_KeyUp(object sender, KeyboardKeyEventArgs e)
        {
            while (keysDown.Contains(e.Key))
                keysDown.Remove(e.Key);
        }
        private void Game_KeyDown(object sender, KeyboardKeyEventArgs e) => keysDown.Add(e.Key);

        public void Update(object sender, FrameEventArgs e)
        {
            keysDownLast = new List<Key>(keysDown);
            buttonsDownLast = new List<MouseButton>(buttonsDown);
        }

        public bool KeyPress(Key key) => (keysDown.Contains(key)) && (!keysDownLast.Contains(key));
        public bool KeyRelease(Key key) => (!keysDown.Contains(key)) && (keysDownLast.Contains(key));
        public bool IsKeyDown(Key key) => keysDown.Contains(key);

        public bool ButtonPress(MouseButton button) => (buttonsDown.Contains(button)) && (!buttonsDownLast.Contains(button));
        public bool ButtonRelease(MouseButton button) => (!buttonsDown.Contains(button)) && (buttonsDownLast.Contains(button));
        public bool ButtonDown(MouseButton button) => buttonsDown.Contains(button);

    }
}
