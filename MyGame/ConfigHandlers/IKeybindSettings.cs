﻿using Config.Net;
using OpenTK.Input;

namespace MyGame.Config
{
    public interface IKeybindSettings
    {
        [Option(DefaultValue = Key.W)]
        Key ForwardKey { set;  get; }

        [Option(DefaultValue = Key.A)]
        Key StrafeLeftKey { set; get; }

        [Option(DefaultValue = Key.S)]
        Key BackwardsKey { set; get; }

        [Option(DefaultValue = Key.D)]
        Key StrafeRightKey { set; get; }

        [Option(DefaultValue = Key.Escape)]
        Key Exit { set; get; }

        [Option(DefaultValue = Key.F)]
        Key ToggleFlying { get; set; }
    }
}
