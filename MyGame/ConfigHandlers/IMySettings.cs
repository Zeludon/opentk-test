﻿using Config.Net;

namespace MyGame.Config
{
    public interface IMySettings
    {
        [Option(DefaultValue = 800)]
        int ScreenWidth { get; }
        [Option(DefaultValue = 600)]
        int ScreenHeight { get; }
    }
}
