﻿using System;
using System.Collections.Generic;
using System.Text;
using Config.Net;
using OpenTK.Input;

namespace MyGame.Config
{
    class KeyParser : ITypeParser
    {
        public IEnumerable<Type> SupportedTypes => new[] { typeof(Key) };

        public string ToRawString(object value)
        {
            if (value == null) return null;

            return ((int)value).ToString();
        }

        public bool TryParse(string value, Type t, out object result)
        {
            if (value == null)
            {
                result = null;
                return false;
            }

            if (t == typeof(Key))
            {
                if (int.TryParse(value, out int c))
                {
                    result = (Key)c;
                    return true;
                }
            }

            result = null;
            return false;
        }
    }
}
