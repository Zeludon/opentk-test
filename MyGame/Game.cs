﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using static OpenTK.MathHelper;
using MyGame.Input;
using Serilog;


namespace MyGame
{
    sealed class Game : GameWindow
    {

        private int VertexBufferID, VertexArrayID;
        private ShaderProgram shaderProgram;
        private Texture2D textureTile;
        private Texture2D textureFace;

        private Camera camera;

        public KeyboardInputHandler KeyInput;



        float[] vertices = {
            -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
             0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
             0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
             0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
            -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
             0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
            -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

            -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
            -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
            -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

             0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
             0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
             0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
             0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
             0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
             0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
             0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
             0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
            -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
            -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

            -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
             0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
             0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
            -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
            -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
        };

        Vector3[] cubePositions = {
          new Vector3( 0.0f,  0.0f,  0.0f),
          new Vector3( 2.0f,  5.0f, -15.0f),
          new Vector3(-1.5f, -2.2f, -2.5f),
          new Vector3(-3.8f, -2.0f, -12.3f),
          new Vector3( 2.4f, -0.4f, -3.5f),
          new Vector3(-1.7f,  3.0f, -7.5f),
          new Vector3( 1.3f, -2.0f, -2.5f),
          new Vector3( 1.5f,  2.0f, -2.5f),
          new Vector3( 1.5f,  0.2f, -1.5f),
          new Vector3(-1.3f,  1.0f, -1.5f)
        };


        public Game(int width, int height) : base(width, height)
        {
            KeyInput = new KeyboardInputHandler(this);
            camera = new Camera(this);

            CursorVisible = false;

        }

        protected override void OnLoad(EventArgs e)
        {
            shaderProgram = new ShaderProgram(@"rect1");

            textureTile = ContentPipe.LoadTexture("tile.jpg");
            textureFace = ContentPipe.LoadTexture("face.png");

            GL.GenVertexArrays(1, out VertexArrayID);
            GL.GenBuffers(1, out VertexBufferID);

            GL.BindVertexArray(VertexArrayID);

            GL.BindBuffer(BufferTarget.ArrayBuffer, VertexBufferID);
            GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(vertices.Length * sizeof(float)), vertices, BufferUsageHint.StaticDraw);

            //GL.BindBuffer(BufferTarget.ElementArrayBuffer, ElementBufferID);
            //GL.BufferData(BufferTarget.ElementArrayBuffer, new IntPtr(indices.Length * sizeof(uint)), indices, BufferUsageHint.StaticDraw);

            // position attribute
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 5 * sizeof(float), IntPtr.Zero);
            GL.EnableVertexAttribArray(0);

            // texture coordinates
            GL.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, false, 5 * sizeof(float), (IntPtr)(3 * sizeof(float)));
            GL.EnableVertexAttribArray(1);

            shaderProgram.Use();
            shaderProgram.SetInt("texture1", 0);
            shaderProgram.SetInt("texture2", 1);
            GL.ActiveTexture(TextureUnit.Texture0);
            textureTile.Use();
            GL.ActiveTexture(TextureUnit.Texture1);
            textureFace.Use();

            base.OnLoad(e);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            GL.ClearColor(Color.FromArgb(25, 25, 25));
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.Enable(EnableCap.DepthTest);

            Matrix4 view = camera.GetViewMatrix();

            Matrix4 projection = Matrix4.CreatePerspectiveFieldOfView(DegreesToRadians(camera.Fov), ClientSize.Width / ClientSize.Height, 0.1f, 100.0f);
            shaderProgram.SetMat4("view", ref view);
            shaderProgram.SetMat4("projection", ref projection);

            GL.BindVertexArray(VertexArrayID);

            for (int i = 0; i < cubePositions.Length; i++)
            {
                Matrix4 model;
                model = Matrix4.CreateTranslation(cubePositions[i]);
                float angle = 10.0f * i;
                model *= Matrix4.CreateFromAxisAngle(new Vector3(1.0f, 0.3f, 0.5f), DegreesToRadians(angle));

                shaderProgram.SetMat4("model", ref model);
                GL.DrawArrays(PrimitiveType.Triangles, 0, 36);
            }

            SwapBuffers();

            base.OnRenderFrame(e);
        }

        protected override void OnResize(EventArgs e)
        {
            GL.Viewport(0, 0, Width, Height);

            base.OnResize(e);
        }

        protected override void OnClosed(EventArgs e)
        {
            GL.DeleteVertexArray(VertexArrayID);
            GL.DeleteBuffer(VertexBufferID);

            base.OnClosed(e);
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            Title = $"(OpenGL Version: {GL.GetString(StringName.Version)}) FPS: {Math.Round(RenderFrequency)}";

            if (KeyInput.KeyPress(KeyInput.KeyBinds.Exit)) Exit();
            if (KeyInput.KeyPress(KeyInput.KeyBinds.ToggleFlying)) camera.ToggleFlying();

            base.OnUpdateFrame(e);
        }

    }
}
