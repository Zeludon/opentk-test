﻿using System.Collections.Generic;
using System.IO;
using OpenTK.Graphics.OpenGL;
using System.Diagnostics;
using OpenTK;

namespace MyGame
{
    sealed class ShaderProgram
    {
        public readonly int ID;

        // constructor reads and builds the shader
        public ShaderProgram(string path)
        {
            var program = GL.CreateProgram();

            var shaders = new List<int>();
            var shadersDir = new DirectoryInfo($@"Content\Shaders\{@path}");
            FileInfo[] files = shadersDir.GetFiles();
            foreach (FileInfo file in files)
            {
                ShaderType type;
                switch (file.Extension)
                {
                    case ".frag":
                        type = ShaderType.FragmentShader;
                        break;
                    case ".vert":
                    default:
                        type = ShaderType.VertexShader;
                        break;
                }
                shaders.Add(CompileShader(type, File.ReadAllText(file.FullName)));
            }

            foreach (var shader in shaders)
                GL.AttachShader(program, shader);

            GL.LinkProgram(program);

            var info = GL.GetProgramInfoLog(program);
            if (!string.IsNullOrWhiteSpace(info))
                Debug.WriteLine($"GL.LinkProgram had info log: {info}");

            foreach (var shader in shaders)
                GL.DeleteShader(shader);

            ID = program;
        }
        // use/activate the shader
        public void Use() => GL.UseProgram(ID);
        
        // utility uniform functions

        public void SetBool(string name, bool value) => 
            GL.Uniform1(GL.GetUniformLocation(ID, name), value ? 1 : 0);

        public void SetInt(string name, int value) =>
            GL.Uniform1(GL.GetUniformLocation(ID, name), value); 
       
        public void SetFloat(string name, float value) =>
            GL.Uniform1(GL.GetUniformLocation(ID, name), value);

        public void SetVec2(string name, ref Vector2 value) =>
            GL.Uniform2(GL.GetUniformLocation(ID, name), value);

        public void SetVec2(string name, float x, float y) =>
            GL.Uniform2(GL.GetUniformLocation(ID, name), x, y);

        public void SetVec3(string name, ref Vector3 value) => 
            GL.Uniform3(GL.GetUniformLocation(ID, name), value);

        public void SetVec3(string name, float x, float y, float z) => 
            GL.Uniform3(GL.GetUniformLocation(ID, name), x, y, z);

        public void SetVec4(string name, ref Vector4 value) => 
            GL.Uniform4(GL.GetUniformLocation(ID, name), value);

        public void SetVec4(string name, float x, float y, float z, float w) => 
            GL.Uniform4(GL.GetUniformLocation(ID, name), x, y, z, w);

        public void SetMat2(string name, ref Matrix2 mat) =>
            GL.UniformMatrix2(GL.GetUniformLocation(ID, name), false, ref mat);

        public void SetMat3(string name, ref Matrix3 mat) => 
            GL.UniformMatrix3(GL.GetUniformLocation(ID, name), false, ref mat);

        public void SetMat4(string name, ref Matrix4 mat) => 
            GL.UniformMatrix4(GL.GetUniformLocation(ID, name), false, ref mat);

        private int CompileShader(ShaderType type, string shaderSrc)
        {
            var shader = GL.CreateShader(type);
            GL.ShaderSource(shader, shaderSrc);
            GL.CompileShader(shader);
            var info = GL.GetShaderInfoLog(shader);
            if (!string.IsNullOrWhiteSpace(info))
                Debug.WriteLine($"GL.CompileShader [{type}] had info log: {info}");
            return shader;
        }
    }
}
