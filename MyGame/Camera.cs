﻿using System;
using OpenTK;
using OpenTK.Input;
using Serilog;
using static OpenTK.MathHelper;

namespace MyGame
{
    enum Camera_Movement
    {
        FORWARD,
        BACKWARD,
        LEFT,
        RIGHT
    };

    class Camera
    {
        // Default camera values
        const float YAW = -90.0f;
        const float PITCH = 0.0f;
        const float SPEED = 2.5f;
        const float SENSITIVITY = 0.2f;
        const float ZOOM = 75.0f;

        // Camera Attributes
        public Vector3 Position = new Vector3(0, 0, 3);
        public Vector3 Front = new Vector3(0, 0, -1);
        public Vector3 Up = Vector3.UnitY;
        public Vector3 Right;
        public Vector3 WorldUp;

        // Camera options
        public float MoveSpeed;
        public float MouseSensitivity;
        public float Fov;
        private bool walking = false;
        private float walkingHeight = 0.0f;
        // Euler Angles
        public float Yaw;
        public float Pitch;

        private Vector2 lastMousePos = new Vector2();

        private Game context;

        public Camera(Game game)
        {
            context = game;

            Yaw = YAW;
            Pitch = PITCH;
            MouseSensitivity = SENSITIVITY;
            WorldUp = Up;
            Fov = ZOOM;

            game.UpdateFrame += Game_UpdateFrame;
            game.FocusedChanged += (sender, e) => { if (context.Focused) resetCursor(); };
            game.MouseWheel += (sender, e) => Fov = MathHelper.Clamp(Fov - e.DeltaPrecise, 45, 90);

            Update();
        }

        private void Game_UpdateFrame(object sender, FrameEventArgs e)
        {
            MoveSpeed = SPEED * (float)e.Time;

            if (context.KeyInput.IsKeyDown(context.KeyInput.KeyBinds.ForwardKey)) Move(Camera_Movement.FORWARD);
            if (context.KeyInput.IsKeyDown(context.KeyInput.KeyBinds.BackwardsKey)) Move(Camera_Movement.BACKWARD);
            if (context.KeyInput.IsKeyDown(context.KeyInput.KeyBinds.StrafeLeftKey)) Move(Camera_Movement.LEFT);
            if (context.KeyInput.IsKeyDown(context.KeyInput.KeyBinds.StrafeRightKey)) Move(Camera_Movement.RIGHT);

            Vector2 delta = lastMousePos - new Vector2(Mouse.GetState().X, Mouse.GetState().Y);

            if (context.Focused && delta != Vector2.Zero)
            {

                Yaw -= delta.X * MouseSensitivity;
                Pitch += delta.Y * MouseSensitivity;

                Pitch = Clamp(Pitch, -89.0f, 89.0f);

                Update();

                resetCursor();
            }
        }

        public Matrix4 GetViewMatrix() => Matrix4.LookAt(Position, Position + Front, Up);

        public void Update()
        {
            Vector3 front = new Vector3()
            {
                X = MathF.Cos(DegreesToRadians(Yaw)) * MathF.Cos(DegreesToRadians(Pitch)),
                Y = MathF.Sin(DegreesToRadians(Pitch)),
                Z = MathF.Sin(DegreesToRadians(Yaw)) * MathF.Cos(DegreesToRadians(Pitch))
            };

            Log.Information("Moving camera to face (x:{X}, y:{Y}, z:{Z})", front.X,front.Y,front.Z);

            Front = front.Normalized();
            Right = Vector3.Cross(Front, WorldUp).Normalized(); // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
            Up = Vector3.Cross(Right, Front).Normalized();
        }

        public void Move(Camera_Movement direction)
        {
            Log.Information("Moving {dir}", direction.ToString());
            if (direction == Camera_Movement.FORWARD)
                Position += Front * MoveSpeed;
            if (direction == Camera_Movement.BACKWARD)
                Position -= Front * MoveSpeed;
            if (direction == Camera_Movement.LEFT)
                Position -= Right * MoveSpeed;
            if (direction == Camera_Movement.RIGHT)
                Position += Right * MoveSpeed;

            if (walking) Position.Y = walkingHeight;
        }

        private void resetCursor()
        {
            Mouse.SetPosition(context.Bounds.Left + context.Bounds.Width / 2, context.Bounds.Top + context.Bounds.Height / 2);
            lastMousePos = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
        }

        public void ToggleFlying()
        {
            Log.Information("Flying set to {walking}", !walking);
            walking = !walking;
            walkingHeight = Position.Y;
            Update();
        }
    }
}
    