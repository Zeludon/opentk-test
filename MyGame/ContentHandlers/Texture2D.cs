﻿using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace MyGame
{
    struct Texture2D {
        private int id;
        private int width, height;

        public int ID => id;
        public int Width => width;
        public int Height => height;

        public Texture2D(int id, int width, int height) {
            this.id = id;
            this.width = width;
            this.height = height;
        }

        public void Use()
        {
            GL.BindTexture(TextureTarget.Texture2D, id);
        }
    }
}
